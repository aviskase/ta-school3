import org.apache.commons.lang3.RandomStringUtils
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.support.ui.ExpectedConditions as Expect
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.annotations.*

import java.nio.file.Paths

/**
 * Created by aviskase on 17/07/16.
 */
class YoutubeTest {
    WebDriver driver
    WebDriverWait wait
    def originalVideoPath = ''
    def videoTitle = 'AviskaseTestVideo'+RandomStringUtils.randomAlphabetic(4)
    def email = 'signuplogintest@gmail.com'
    def password = 'logintest'
    def homeURL = 'https://www.youtube.com/'

    @BeforeClass
    void setUp() {
        try {
            originalVideoPath = Paths.get(getClass().getResource('/video.avi').toURI()).toString()
        } catch (URISyntaxException e) {
            e.printStackTrace()
        }
        System.setProperty 'webdriver.chrome.driver', '../chromedriver'
        ChromeOptions options = new ChromeOptions()
        def prefs = [:]
        // Убираем спрашивалку нотификаций
        prefs.put 'profile.default_content_setting_values.notifications', 2
        // Сохраняем закачки в директорию проекта
        prefs.put 'download.default_directory', System.getProperty('user.dir')
        options.setExperimentalOption 'prefs', prefs

        driver = new ChromeDriver(options)
        wait = new WebDriverWait(driver, 20)

        driver.get homeURL
        def signInButtonLocator = '//*[contains(@class, "signin-container")]/button'
        def signInButton = wait.until(Expect.elementToBeClickable(By.xpath(signInButtonLocator)))
        signInButton.click()
        def emailField = wait.until(Expect.visibilityOfElementLocated(By.id('Email')))
        emailField.clear()
        emailField.sendKeys email
        emailField.sendKeys Keys.RETURN
        def passwordField = wait.until(Expect.visibilityOfElementLocated(By.id('Passwd')))
        passwordField.clear()
        passwordField.sendKeys password
        passwordField.sendKeys Keys.RETURN
    }

    @Test
    public void testVideoIsUploadedCorrectly() throws Exception {
        driver.get homeURL
        def uploadButton = wait.until(Expect.elementToBeClickable(By.id("upload-btn")))
        uploadButton.click()
        def uploadBox = wait.until(Expect.visibilityOfElementLocated(By.id('upload-prompt-box')))
        def uploadField = uploadBox.findElement(By.xpath('input[@type="file"]'))
        uploadField.sendKeys originalVideoPath

        // #1 Видео загрузилось
        wait.until Expect.attributeToBe(
                By.className('progress-bar-uploading'),
                'aria-valuenow',
                '100'
        )

        def titleLocator = '//*[@name="title"]'
        def titleField = driver.findElement(By.xpath(titleLocator))
        titleField.clear()
        titleField.sendKeys videoTitle

        def publishButtonLocator = '//*[@id="active-uploads-contain"]//*[contains(@class, "metadata-save-button")]//button'
        def publishButton = wait.until(Expect.elementToBeClickable(By.xpath(publishButtonLocator)))
        publishButton.click()

        // #2 Видео обработалось
        wait.until Expect.attributeToBe(
                By.className('progress-bar-processing'),
                'aria-valuenow',
                '100'
        )

        // #3 Дана ссылка на видео
        def shareField = wait.until(Expect.visibilityOfElementLocated(
                By.name('share_url')
        ))

        // #4 Ссылка рабочая
        def shareURL = shareField.getAttribute('value')
        driver.get shareURL
        def title = wait.until(Expect.visibilityOfElementLocated(
                By.id('eow-title')
        ))
        assert title.text == videoTitle
    }

    @Test(dependsOnMethods = ['testVideoIsUploadedCorrectly'])
    public void testSearchUploadedVideo() throws Exception {
        // Хоть тресни, 10 секунд ждать. От размера видео зависимости особой нет.
        // Есть подозрение, что связано с моим нынешним местоположением.
        // Ходили легенды, что тут есть проблемы с ютубовскими серверами.
        sleep 12000
        driver.get homeURL

        def searchField = wait.until(Expect.visibilityOfElementLocated(By.id('masthead-search-term')))
        searchField.clear()
        searchField.sendKeys videoTitle
        searchField.sendKeys Keys.RETURN

        def results = wait.until(Expect.visibilityOfElementLocated(By.id('results')))
        def titles = results.findElements(By.xpath('//h3/a'))
        assert titles.any { it.text == videoTitle }
    }

    @Test
    public void testChangeCurrentLanguage() throws Exception {
        changeLang 'kk'
        def pageLang = driver.findElement(By.tagName('html')).getAttribute('lang')
        assert pageLang == 'kk'
    }


    @AfterClass
    public void tearDown() {
        deleteAllUploadedVideos()
        changeLang 'en'

        driver.close()
        driver.quit()
    }

    private void deleteAllUploadedVideos() {
        driver.get homeURL + 'my_videos?o=U'
        wait.until Expect.visibilityOfElementLocated(
                By.className('vm-video-item-content')
        )
        def selectAllLocator = '//*[@id="non-appbar-vm-video-actions-bar"]//input[@type="checkbox"]'
        driver.findElement(By.xpath(selectAllLocator)).click()
        def actionMenuLocator = '//*[@id="non-appbar-vm-video-actions-bar"]//button[contains(@class, "vm-video-list-action-menu")]'
        driver.findElement(By.xpath(actionMenuLocator)).click()
        def deleteMenuLocator = '.non-appbar-action-menu-content button.vm-actions-bar-delete-videos'
        driver.findElement(By.cssSelector(deleteMenuLocator)).click()

        def confirmDeleteButton = wait.until(Expect.elementToBeClickable(
                By.className('vm-video-actions-delete-button-confirm')
        ))
        confirmDeleteButton.click()
    }

    private void changeLang(String language) {
        def pageLang = driver.findElement(By.tagName('html')).getAttribute('lang')
        if (pageLang == language) {
            println "Language $language is already selected"
            return
        }
        driver.get homeURL
        def changeLanguageButton = wait.until(Expect.elementToBeClickable(
                By.id('yt-picker-language-button')
        ))
        changeLanguageButton.click()
        def languageFormLocator = '//*[@id="yt-picker-language-footer"]//form'
        wait.until Expect.visibilityOfElementLocated(
                By.xpath(languageFormLocator)
        )
        def languageButtonLocator = "//*[@id='yt-picker-language-footer']//button[@value='$language']"
        driver.findElement(By.xpath(languageButtonLocator)).click()
    }
}
