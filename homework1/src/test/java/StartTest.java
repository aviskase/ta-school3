
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import org.apache.commons.lang3.RandomStringUtils;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


/**
 * Created by aviskase on 14/07/16.
 */

public class StartTest {
    WebDriver driver;
    WebDriverWait wait;
    String originalImagePath = "";
    String downloadedImagePath = "";
    String email = "signuplogintest@gmail.com";
    String password = "logintest";

    @BeforeClass
    public void setUp() {
        try {
            originalImagePath = Paths.get(getClass().getResource("/dummy-image.png").toURI()).toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        // Показываем, где лежит драйвер на хром
        System.setProperty("webdriver.chrome.driver", "../chromedriver");

        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        // Убираем спрашивалку нотификаций
        prefs.put("profile.default_content_setting_values.notifications", 2);
        // Сохраняем закачки в директорию проекта
        prefs.put("download.default_directory", System.getProperty("user.dir"));
        options.setExperimentalOption("prefs", prefs);

        driver = new ChromeDriver(options);

        driver.get("https://www.facebook.com/");
        WebElement emailField = driver.findElement(By.id("email"));
        emailField.clear();
        emailField.sendKeys(email);
        WebElement passwordField = driver.findElement(By.id("pass"));
        passwordField.clear();
        passwordField.sendKeys(password);
        passwordField.sendKeys(Keys.RETURN);

        wait =  new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pagelet_navigation")));
    }

    @Test()
    public void createGroupAndAlbumAndUploadImage() {
        // Группировка чисто логическая, т.к. методы зависят друг от друга
        createGroup();
        createAlbum();
        downloadImage();
        Assert.assertTrue(imagesAreEqual(originalImagePath, downloadedImagePath));
    }

    private void createAlbum() {
        String attachLinkLocator = "//a[@data-testid='media-attachment-selector']";
        WebElement attachLink = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(attachLinkLocator)));
        attachLink.click();

        String photoInputLocator = "//input[@data-testid='media-attachment-add-photo']/following::input[@type='file']";
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(photoInputLocator+"/ancestor::a[2]")));
        WebElement photoInput = driver.findElement(By.xpath(photoInputLocator));
        photoInput.sendKeys(originalImagePath);

        String createAlbumButtonLocator = "//*[contains(@class, 'albumPlaceTypeahead')]/following::button[@type='submit']";
        WebElement createAlbumButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(createAlbumButtonLocator)));
        createAlbumButton.click();
    }

    private void createGroup() {
        String createGroupLinkLocator = "//a[@data-testid='left_nav_item_Create Group']";
        driver.findElement(By.xpath(createGroupLinkLocator)).click();

        String groupNameLocator = "//*[@id='groupsCreateName']/following::input[@name='name']";
        WebElement groupName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(groupNameLocator)));
        groupName.clear();
        groupName.sendKeys("TestGroup-"+ RandomStringUtils.randomAlphanumeric(4));

        String groupMembersLocator = "//*[@id='groupsCreateMembers']/following::input[@type='text']";
        WebElement members = driver.findElement(By.xpath(groupMembersLocator));
        members.sendKeys(email);
        String memberLocator = "//*[@id='groupsCreateMembers']/following::li[@title='" + email+ "']";
        WebElement member = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(memberLocator)));
        member.click();

        driver.findElement(By.id("privacy_3")).click();

        String createGroupButtonLocator = "//*[@id='groupsCreateName']/following::button[@type='submit']";
        WebElement createGroupButton = driver.findElement(By.xpath(createGroupButtonLocator));
        createGroupButton.click();

        String gotoGroupPageButtonLocator = "//*[@id='groupIconListIconID']/following::button[@type='submit']";
        WebElement gotoGroupPageButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(gotoGroupPageButtonLocator)));
        gotoGroupPageButton.click();
    }

    private void downloadImage() {
        WebElement albumPage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("set_photos_pagelet")));
        albumPage.findElement(By.className("uiMediaThumb")).click();

        String optionButtonLocator = "//*[@data-action-type='open_options_flyout']";
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(optionButtonLocator)));

        WebElement imageRegion = driver.findElement(By.className("stage"));
        Actions builder = new Actions(driver);
        builder.moveToElement(imageRegion).perform();
        driver.findElement(By.xpath(optionButtonLocator)).click();
        String downloadButtonLocator = "//*[@data-action-type='download_photo']";
        WebElement downloadButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(downloadButtonLocator)));
        downloadButton.click();

        // Типо дождаться загрузки. Почему-то иногда валилось без ожидания.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Найти скачанную картинку
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(System.getProperty("user.dir")), "*.jpg")) {
            downloadedImagePath = stream.iterator().next().toAbsolutePath().toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean imagesAreEqual(String originalImagePath, String downloadedImagePath) {
        // Сравниваем с помощью imagemagick / compare
        String[] command = new String[] {"compare", "-metric", "MSE", originalImagePath, downloadedImagePath, "NULL:"};
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.redirectErrorStream(true);
        Process process = null;
        try {
            process = processBuilder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scanner std = new Scanner(process.getInputStream());
        double difference = std.nextDouble();
        std.close();

        // Немного магическое число =)
        return difference < 5;
    }

    @AfterClass
    public void cleanUp() {
        driver.close();
        driver.quit();

        // Удаляем скачанную картинку
        Paths.get(downloadedImagePath).toFile().delete();
    }
}
